/**
  API API joy joy
  API to API interface
  Ideal result: Matches an incoming API to an outgoing API, translates from one to the other via table / hash entry that distills both side to a generic function and translates
  APIs to (eventually) be supported are: Google RESTful API, Smartsheet API, Atlassian API, more...
  This is meant to be standalone and eventually become a library - so it cannot be dependent on sheets/forms/slides UIs which presents challenges
  Also challenging to work around Apps Scripts Quotas/Limits 
**/

//********************** Class Declarations *********************************//
class MetaUser {
  constructor () {
    //In Future should scan sheet header and assign columns dynamically
    const row_Header = 1;
    const col_Name = 1;
    const col_Level = 2;
    const col_Group = 3;

    var props_thisApp = PropertiesService.getScriptProperties();

    this.email = Session.getActiveUser().getEmail();
    this.name = Session.getActiveUser().getUserLoginId();


  }


}

class PropertiesCollection {

  constructor(app_properties, api_properties) {
    /**
     * Expected: app_properties is an array containing properties objects from apps
     * This creates a single properties object from calling app and api interface
    */
    this.doc = PropertiesService.getDocumentProperties();
    this.user = PropertiesService.getUserProperties();
    //Now need to loop through app properties and api properties

  }


}

//**************** API STRUCTURE / INITIALIZATION FUNCTIONS ****************//

//Resets properties and finds API keys for this project
function resetProperties () {

  //This should only have to be run once, but if the API Key sheet gets moved and ID changed, or someone else takes this over, or something just BREAKS this may need to be run again
  //This needs to be run from the master project or from an installable trigger as an owner and admin group user in order to have the correct permissions, if ran as normal user, will fail

  var props = {};
  props.user = PropertiesService.getUserProperties();
  props.thisApp = PropertiesService.getScriptProperties(); //This should get properties specifically for ApiApiJoyJoy Script - not the calling script

  var debug = (props.thisApp.getProperty('Debug Mode') == 'True');

  //Sets the Google Drive ID of the access list
  //Eventually there needs to be an initialization function / install script that will set this, in case this is adopted at another organization
  var id_shtAdmin = props.thisApp.getProperty('Admin Sheet ID');
  if (id_shtAdmin == null) {
    id_shtAdmin = '18qIIQSrPIF52XGNW2uI9cmATlE6KflZbnIhFOmvR0SM'; //Sheet ID for list of valid users and admins. Future: Should be a script property not explicitly shown here, and should prompt for entry if it doesn't exist.
    props.thisApp.setProperty('Admin Sheet ID',id_shtAdmin);
  }
  
  //Sets the name of the project
  props.thisApp.setProperty('App Name','ApiApiJoyJoy');
  prop_Script.setProperty('Debug Mode','False');

  //Create User Object for current user (when ran on a trigger as necessary, will be SuperUser / Admin)
  var usr_Current = getUserObject();

  //Run if user is verified Admin
  if (usr_Current.group = 'Admin') {
    
    props.superUser = props.user;
    var usr_Super = usr_Current;

    //Deletes all properties (in case property got corrupted)
    props.thisApp.deleteAllProperties();
    props.superUser.deleteAllProperties();
    //Note: User Properties control access and should not be reset except in extreme circumstances
    //Note: Document Properties cannot be accessed from library functions

    props.thisApp.setProperty('Admin Sheet ID', id_shtAdmin); //Reset the Admin Sheet ID
    props.thisApp.setProperty('Last Reset Version',getVersion());
    
    //Sets the Google Drive ID of the public Key Sheet
    props.superUser.setProperty('Public Key Sheet ID','1DpL3VgeaI4REiAuhHCesO2z5rUl9FZXaXicB34KQaBs'); //Public Sheet ID for sheet storing SmartSheet API ID and ID of secret key sheet. Future: Should be a script property, and should prompt for entry if it doesn't exist.
    props.superUser.setProperty('Google Access Level',usr_Super.googleaccess);
    props.superUser.setProperty('Organization Access Group',usr_Super.group);

    //Resets the credentials based on the key sheet
    var creds = findAPIcredentials(usr_Super);

    //This is currently generic. Will need to update to do separate for different WebApps - SmartSheets, Google, etc.
    if (creds.success) {

      //Properties are now set in findAPIcredentials, no need to separately call here
      if (debug) {console.log('Found following credentials: ID: ', creds.id, ' Secret: ', creds.secret)}
      //props.superUser.setProperty('Client ID', creds.id);
      //props.superUser.setProperty('Client Secret', creds.secret);

    } else {
      
      //Error, always log
      console.log('Failed finding user credentials for API')

    }

  } else {

    //Access / Security Issue - always log
    var txt_error = 'Non-Admin User ' + usr_Current.email + ' attempted to access admin function ' + arguments.callee.name;
    console.log(txt_error);
    throw txt_error;

  }

}

function getVersion() {

  //Reports version of ApiApiJoyJoy
  const ver_API = '0.5';
  return ver_API;

}

function modeDebug() {

  let props_thisApp = PropertiesService.getScriptProperties();
  let debug = (props_thisApp.getProperty('Debug Mode') == 'True');
  return debug;

}

function checkProperties(debug) {

  var props = {};
  props.thisApp = PropertiesService.getScriptProperties();
  props.user = PropertiesService.getUserProperties();
  
  if (debug == null) {

    debug = (props.thisApp['Debug Mode'] == 'True');

  }

  if (debug) {

    console.log('Script Properties:');
    console.log(props.thisApp.getProperties());

    console.log('User Properties:');
    console.log(props.user.getProperties());

  }

  return props.thisApp;

}

/**
 * This function should be in the calling App
 * -OR-
 * This function needs to be split between
 * ApiApiJoyJoy and the calling App
 * Such that all forms are handled in the calling app
 * This is a violation of the scope boundary
 * All forms should be handled in the calling app
 */
function showPropertyForm(str_name, hash_Fields) {
  
  const int_defaultTableWidth = 1024;
  const int_defaultMaxTextLength = 16;
  const int_defaultSubmitButtonSize = 8;
  const isForm = true;
  const isElementFixed = true;
  const isHeader = false;
  const isNewFixed = false;

  const cmd_submit = 'google.script.run.setProperties(this); google.script.host.close();';
  
  var debug = modeDebug();

  //This only works specifically for user, document, and script properties:
  var name_elements = Object.keys(hash_Fields);
  if (debug) {console.log(name_elements)};

  var int_TableWidth = int_defaultTableWidth;
  var int_MaxTextLength = int_defaultMaxTextLength;
  var int_SubmitButtonSize = int_defaultSubmitButtonSize;

  //Thought on HTML formatting, could track a counter for indent and use that to add spaces at beginning to be more dynamic
  var str_HTMLtable = '        <table style="width: ' + int_TableWidth + 'px; table-layout:fixed;" align="center">\n';
  var thiskey = '';
  var thisvalue = '';

  //Scan for longest value - must be a more efficient way to do this than loop through twice?
  for (var k=0; k<name_elements.length; k++) {
  
    thiskey = name_elements[k];
    thisvalue = scrubText(hash_Fields[thiskey]); //This string needs to get scrubbed not just to prevent errors but also to prevent malicious strings/commands from being passed

    if (thisvalue.length > int_MaxTextLength) {int_MaxTextLength = thisvalue.length}
    hash_Fields[thiskey] = thisvalue; //Set the property as the scrubbed version of the value

  }

  //Now actually populate the table
  for (var k=0; k<name_elements.length; k++) {
  
    thiskey = name_elements[k];
    thisvalue = hash_Fields[thiskey];
    if (debug) {
      console.log('Adding Element');
      console.log(thiskey);
    }
    str_HTMLtable = str_HTMLtable + addHTMLRow(thiskey, thisvalue, isForm, isElementFixed, isHeader, int_MaxTextLength);

  }

  //Add Line for a new value
  str_HTMLtable = str_HTMLtable + addHTMLRow('new','', isForm, isNewFixed, isHeader, int_MaxTextLength);
  var str_buttonSubmit = '<input type="submit" name="submit" value="Submit" size="' + int_SubmitButtonSize + '">'; 
  str_HTMLtable = str_HTMLtable + addHTMLRow('',str_buttonSubmit); //Add submit button
  str_HTMLtable = str_HTMLtable + "        </table>\n";

  showForm(cmd_submit, str_HTMLtable, str_name);

}

/**
 * This function should be in the calling App
 * -OR-
 * This function needs to be split between
 * ApiApiJoyJoy and the calling App
 * Such that all forms are handled in the calling app
 * This is a violation of the scope boundary
 * All forms should be handled in the calling app
 */
function editHiddenProperties(type, props_Gen) {
  
  //props_Gen is required if attempting to edit Document Properties, otherwise is ignored
  props = getProps(type, props_Gen);

  const int_defaultTableWidth = 1024;
  const int_defaultMaxTextLength = 16;
  
  const int_defaultSubmitButtonSize = 8;
  const isForm = true;
  const isElementFixed = true;
  const isNewFixed = false;
  const isHeader = false;

  const cmd_submit = 'google.script.run.setProperties(this); google.script.host.close();';
  
  var debug = modeDebug();

  //This only works specifically for user, document, and script properties:
  var name_elements = props.getKeys();
  //This should work for ALL property collections:
  //var name_elements = Object.keys(props);
  if (debug) {console.log(name_elements)}

  var int_TableWidth = int_defaultTableWidth;
  var int_MaxTextLength = int_defaultMaxTextLength;
  var int_SubmitButtonSize = int_defaultSubmitButtonSize;

  //Thought on HTML formatting, could track a counter for indent and use that to add spaces at beginning to be more dynamic
  var str_HTMLtable = '        <table style="width: ' + int_TableWidth + 'px; table-layout:fixed;" align="center">\n';
  var thiskey = '';
  var thisvalue = '';

  //Scan for longest value - must be a more efficient way to do this than loop through twice?
  for (var k=0; k<name_elements.length; k++) {
  
    thiskey = name_elements[k];
    thisvalue = scrubText(props.getProperty(thiskey)); //This string needs to get scrubbed not just to prevent errors but also to prevent malicious strings/commands from being passed

    if (thisvalue.length > int_MaxTextLength) {int_MaxTextLength = thisvalue.length}
    props.setProperty(thiskey, thisvalue); //Set the property as the scrubbed version of the value

  }

  //Now actually populate the table
  for (var k=0; k<name_elements.length; k++) {
  
    thiskey = name_elements[k];
    thisvalue = props.getProperty(thiskey);
    if (debug) {
      console.log('Adding Element');
      console.log(thiskey);
    }
    str_HTMLtable = str_HTMLtable + addHTMLRow(thiskey, thisvalue, isForm, isElementFixed, isHeader, int_MaxTextLength);

  }

  //Add Line for a new value
  str_HTMLtable = str_HTMLtable + addHTMLRow('new','', isForm, isNewFixed, isHeader, int_MaxTextLength);
  var str_buttonSubmit = '<input type="submit" name="submit" value="Submit" size="' + int_SubmitButtonSize + '">'; 
  str_HTMLtable = str_HTMLtable + addHTMLRow('',str_buttonSubmit); //Add submit button
  str_HTMLtable = str_HTMLtable + "        </table>\n";

  showForm(cmd_submit, str_HTMLtable, type);

}

function setHiddenProperties (form, props_Gen) {

  props = getProps(form.type, props_Gen);

  var formkeys = Object.keys(form);
  var thiskey = new String;

  var debug = modeDebug();
  if (debug) {console.log(form)}

  //Loop through elements and if and when form element keys match, change to the form's properties
  for(var k=0; k<formkeys.length; k++) {

    thiskey = formkeys[k];
    //Some keys, like "type" are not actual stored properties, but no specific checks are needed as those properties would return null anyway
    if (props.getProperty(thiskey) != null && props.getProperty(thiskey) != '') { //Future: Need better check, use regexp to make sure at least one non-whitespace character

      props.setProperty(thiskey, form[thiskey]);

    } else {

      if (thiskey == 'new_key' && form[thiskey] != null && form[thiskey] != ''){ //Special Case of new_key to create a newkey

        props.setProperty(form[thiskey], form['new']);

      }

    }

  }

}

function getProps(type, props_Gen) {

  switch (type) {

    case null:
      throw 'No Type Sent';
      break;

    case 'Script':
      var props = PropertiesService.getScriptProperties();
      break;

    case 'User':
      var props = PropertiesService.getUserProperties();
      break;

    case 'Document':
      if (props_Gen != null) {
        var props = props_Gen;
      } else {
        //Cannot access Document Properties unless they are passed to the library script
        throw 'Cannot Access Document Properties without passing from calling app';        
      }
      break;

    default:
      if (props_Gen != null) {
        var props = props_Gen;
      } else {
        //Cannot access Document Properties unless they are passed to the library script
        throw 'Cannot Access Document Properties without passing from calling app';        
      }
      //Whether 'Document' Type or another, not specifically defined type, need properties passed directly to this. Non propertieservice types do not need .getproperties

  }

  return props;

}

function scrubText(str_unclean, debug) {

  if (debug == null) {debug = modeDebug()}

  //Need to retain all alphanumeric characters, the "@" sign and spaces others are invalid and must go
  str_clean = str_unclean.replace(/"|'/g, "");
  //str_clean = str_unclean;
  if (debug) {
    console.log('Scrubbing:');
    console.log(str_clean);
  }

  return str_clean;

}

/**
 * This function should be in the calling App
 * -OR-
 * This function needs to be split between
 * ApiApiJoyJoy and the calling App
 * Such that all forms are handled in the calling app
 * This is a violation of the scope boundary
 * All forms should be handled in the calling app
 */
function showForm(cmd_submit, str_HTMLtable, str_type, int_FormWidth, int_FormHeight) {

  var ui = SpreadsheetApp.getUi();
  //var temp_frm = HtmlService.createTemplateFromFile('Properties');
  //temp_frm.runonsubmit = cmd_submit;
  //temp_frm.propertiestable = str_HTMLtable;
  
  var debug=modeDebug();

  //Get actual template code in logger to debug
  if (debug) {console.log(temp_frm.getCode())}

  //var frm_HTML = temp_frm.evaluate().setSandboxMode(HtmlService.SandboxMode.IFRAME).setXFrameOptionsMode(HtmlService.XFrameOptionsMode.ALLOWALL);
  //var frm_HTML = HtmlService.createHtmlOutput('<b>Hello, world!</b>');
  const int_defaultFormWidth = 1200;
  const int_defaultFormHeight = 400;
  const str_frmStart = '    <form id="Properties" action="javascript:void(0)" onsubmit="' + cmd_submit + '">\n';
  const str_frmType = '      <input type="hidden" name="type" value="' + str_type + '"/>\n';
  const str_frmEnd = '    </form>\n';
  
  //If optional parameters are not used, set to default values
  var str_Title = str_type.toUpperCase() + " Properties";
  if (int_FormWidth == null) {int_FormWidth = int_defaultFormWidth}
  if (int_FormHeight == null) {int_FormHeight = int_defaultFormHeight}

  var frm_HTML = HtmlService.createHtmlOutput(str_frmStart);
  frm_HTML.append(str_frmType); //Adds type as hidden parameter
  frm_HTML.append(str_HTMLtable);
  frm_HTML.append(str_frmEnd);

  //Future: Need to check width x height of the window, and limit size if Window is too small for full size
  //May want entirely independent function / class to calculate the necessary size of the window and the table elements within with priority rules
  frm_HTML.setWidth(int_FormWidth);
  //console.log('Width Set to ' + int_FormWidth);
  frm_HTML.setHeight(int_FormHeight);

  //Get actual HTML code in logger to debug
  if (debug) {console.log(frm_HTML.getContent())}

  var newprops = ui.showModalDialog(frm_HTML,str_Title);

  return newprops;

}

/**
 * This function should be in KISSHTML
 * -OR-
 * This function needs to be split between
 * ApiApiJoyJoy and KISSHTML
 * Such that all HTML form building is handled in KISSHTML
 * This is a violation of the scope boundary
 * All HTML generation should be handled by KISSHTML
 */
function addHTMLRow(thiskey, thisvalue, isForm, isFixed, isHeader, int_MaxTextLength, int_FontSize, int_Cushion, int_LeftWidth, int_RightWidth){

  //Set Default Values
  const bool_defaultisForm = false;
  const bool_defaultisFixed = true; //Fixed by default, user won't be able to change the key name
  const bool_defaultisHeader = false;

  const int_defaultFontSize = 16; //Default font size in pixels
  const int_defaultCushion = 8; //Default minimum cushion. Addsing to the cell size for extra margin around the input box
  const int_defaultMaxTextLength = 16;
  const int_defaultLeftWidth = 200;
  //const int_defaultRightWidth = int_defaultMaxTextLength * int_defaultFontSize + int_defaultCushion; //Calculated, but unchanging. Nice feature of Javascript. C wouldn't allow this

  //Check Optional Parameters, set to default if not passed
  if (isForm == null) {isForm = bool_defaultisForm}
  if (isFixed == null) {isFixed = bool_defaultisFixed}
  if (isHeader == null) {isHeader = bool_defaultisHeader}
  if (int_FontSize == null) {int_FontSize = int_defaultFontSize}
  if (int_Cushion == null) {int_Cushion = int_defaultCushion}
  if (int_MaxTextLength == null) {int_MaxTextLength = int_defaultMaxTextLength}
  if (int_RightWidth == null) {int_RightWidth = int_MaxTextLength * int_FontSize + int_Cushion}  
  if (int_LeftWidth == null) {
   
    //if (isForm && !isFixed) {
    //  int_LeftWidth = int_RightWidth; //If it's a form and not fixed, make same as right
    //} else {
      
      int_LeftWidth = int_defaultLeftWidth
    
    //}

  }

  var int_elements = 1;
  var haskey = false;
  var valueisArray = Array.isArray(thisvalue);

  if (valueisArray) {

    int_elements = thisvalue.length;

  }
  
  if (thiskey != null && thiskey != '') {
      int_elements++;

      if (valueisArray) {

        thisvalue.unshift(thiskey)  //Adds key name passed to begging of value array and shifts all by 1 element to match int_elements

      } else {

        haskey = true; //if valueisArray, then the "key" value was added to the beginning of thisvalue and we can assume false

      }

  }

  //Future: Set error if required parameters not passed

  var int_inputSize = parseInt((int_RightWidth / int_FontSize) + 1); //Supposedly default browser size is 16 pixels per character - will need to make more sophisticated later with CSS
  var int_newInputSize = parseInt((int_LeftWidth / int_FontSize) + 1); //Both are rounding up
  //console.log(int_RightWidth);
  //console.log(int_inputSize);
  var str_leftCell = thiskey;
  var str_RightCell = thisvalue;

  //Future: Simplify this using regular expressions / Patterns
  if (isForm) { //If it's a form, the value can be changed

    if (isFixed) { //If it's not fixed, the key name can also change

      str_leftCell = '<label for="' + thiskey + '">' + thiskey + '</label>';

    } else if (thiskey.toLowerCase() == 'new') { //Special condition

      str_leftCell = '<input type ="text" name="' +thiskey + '_key" value="" size="' + int_newInputSize + '"/>';

    } else {

      str_leftCell = '<input type ="text" name="' + thiskey + '_key" value="' + thiskey + '<" size="' + int_newInputSize + '"/>';
    
    }

    str_RightCell = '<input type="text" name="' + thiskey + '" value="' + thisvalue + '" size="' + int_inputSize + '"/>';

  }

  var str_return = '            <tr> ';
  var str_cell = new String;
  for (let e=0;e<int_elements;e++) {

    if (e == 0 && haskey) {
      
      //Always a header in this case
      str_cell = addHTMLCell(str_leftCell, int_LeftWidth, true);
    
    } else {

      //Header based on value passed in to function
      if (Array.isArray(str_RightCell)) {
        str_cell = addHTMLCell(str_RightCell[e],int_RightWidth, isHeader);
      } else {

        str_cell = addHTMLCell(str_RightCell,int_RightWidth, isHeader);

      }

    }
    str_return = str_return + str_cell;
  }

  //str_return = "            <tr> " + addHTMLCell(str_leftCell, int_LeftWidth) + addHTMLCell(str_RightCell, int_RightWidth) + " </tr>\n";
  //console.log(str_return);

  return str_return;

}

/**
 * This function should be in KISSHTML
 * -OR-
 * This function needs to be split between
 * ApiApiJoyJoy and KISSHTML
 * Such that all HTML form building is handled in KISSHTML
 * This is a violation of the scope boundary
 * All HTML generation should be handled by KISSHTML
 */
function addHTMLCell(contents, int_CellWidth, isHeader){
  
  //Set Default Values
  const int_defaultCellWidth = 100;
  const bool_defaultisHeader = false;

  //Check Optional Paramters, set to default if not passed
  if (int_CellWidth == null) {int_CellWidth = int_defaultCellWidth}
  if (isHeader == null) {isHeader = bool_defaultisHeader}

  if (isHeader) {
    str_celltype = 'th';
  } else {
    str_celltype = 'td';
  }
  var str_HTMLcell = '<' + str_celltype + ' style="width: ' + int_CellWidth + 'px" > ' + contents + ' </' + str_celltype + '>';

  return str_HTMLcell;

}


function getUserObject (useremail) {

  //In Future should scan sheet header and assign columns dynamically
  const row_Header = 1;
  const col_Name = 1;
  const col_Level = 2;
  const col_Group = 3;

  const thisuser = {};

  //Future: Should also include OAuth2 authorizations
    //if (ScriptApp.getAuthorizationInfo(ScriptApp.AuthMode.FULL).getAuthorizationStatus() == ScriptApp.AuthorizationStatus.NOT_REQUIRED) {
    // user has authorized the OAuth scopes
  //}

  var props_thisApp = PropertiesService.getScriptProperties();

  var name_Groups = 'admin_Groups';
  //var name_Access = 'ACCESS';

  var thisss = SpreadsheetApp.openById(props_thisApp.getProperty('Admin Sheet ID'));
  var sht_Groups = thisss.getSheetByName(name_Groups);

  //Get current user name/email
  if (useremail == undefined) {
    //If no argument passed, assume the current active user
    thisuser.email = Session.getActiveUser().getEmail();
    console.log('Current User is: ' + thisuser.email);

  } else {
    //If argument passed, use the passed argument
    thisuser.email = useremail;
  }

  var user_found = false;

  for(var r=row_Header + 1; r <= sht_Groups.getLastRow(); r++) {

    //Search user list in groups sheet for a match with current user
    if (sht_Groups.getRange(r,col_Name).getValue() == thisuser.email) {

      user_found = true;
      //If user is found, set object values to associated columns in the sheet
      thisuser.googleaccess = sht_Groups.getRange(r,col_Level).getValue();
      thisuser.group = sht_Groups.getRange(r,col_Group).getValue();

    }

  }

  if (!user_found) {

    //If user is not found, set values to 'Not Found'
    thisuser.googleaccess = 'Not Found';
    thisuser.group = 'Not Found';

  }

  return thisuser;

}

function findAPIcredentials(obj_User) {

  //Steps in final implementation should be as follows:
  //1. Retrieve current user information
  //2. Verify User is MetaProject / ApiApiJoyJoy Admin
  //3. Lookup Department API Sheet from hardcoded ID - match username & project name(MetaProject or ApiApiJoyJoy)
  //4. Get user API sheet ID from same row
  //5. Lookup User API sheet from sheet ID - again match username & project name
  //6. Import Client ID and Client Secret from same row

  //Set up response object
  var creds = {};
  creds.id = null;
  creds.secret = null;
  creds.success = false;
  
  //Constants
  const row_header = 1;

  const name_shtKeys = 'KeyTable';
  const max_cols = 127;
  
  var props = {};
  props.superUser = PropertiesService.getUserProperties();
  props.thisApp = PropertiesService.getScriptProperties();
  var ss_PublicKeys = SpreadsheetApp.openById(props.superUser.getProperty('Public Key Sheet ID'));
  var sht_PublicKeys = ss_PublicKeys.getSheetByName(name_shtKeys);

  var debug = (props.thisApp.getProperty('Debug Mode') == 'True');

  //1. Retrieve current user information

  //As part of this installable trigger, these are created as the script developer, and are not visible to the sheet user otherwise
  //These are necessary to fetch secret API keys without other users seeing
  //In order to change developers in the future, however, will need to edit API Keys sheet
  //New owner would have to make a new secret key in SmartSheet to add to API keys sheets per instructions in sheet
  
  if (obj_User == null) {obj_User = getUserObject()} //Only call if user object was not passed
  

  //2. Verify User is MetaProject / ApiApiJoyJoy Admin

  if (obj_User.group.toLowerCase() == 'admin') {

    var usr_Super = obj_User //User verified as super user;
  
  } else {

    //Always log as this is an error
    var txt_error = 'Non-Admin User ' + props.superUser.email + ' attempted to access admin function ' + arguments.callee.name;
    console.log(txt_error);
    throw txt_error; //This sets an error in the console and immediately exits function

  }

  //3. Lookup Department API Sheet from hardcoded ID - match username & project name(MetaProject or ApiApiJoyJoy)
  var c = 1;
  var usercolfound = false;
  var appcolfound = false;
  var shtidcolfound = false;
  var idcolfound = false;
  var secretcolfound = false;
  var blankcolfound = false;
  var allfound = false;

  //Ideally, instead of a loop, we'll have a hash eventually

  while ((!usercolfound || !appcolfound || !shtidcolfound || !idcolfound || !secretcolfound) && !blankcolfound && c < max_cols) {
	  
    val_current = sht_PublicKeys.getRange(row_header,c).getValue();

	  switch (val_current) {
      case 'User':
    
		    usercolfound = true;
		    var col_user = c;
        if (debug) {console.log('user found at column ' + c)}

        break;
		  
      case 'App Name':

        appcolfound = true;
        var col_app = c;
        if (debug) {console.log('app found at column ' + c)}

        break;
    
      case 'User Key Sheet ID':

        shtidcolfound = true;
        var col_shtid = c;
        if (debug) {console.log('User Sheet ID found at column ' + c)}

        break;

      case 'Client ID':

        idcolfound = true;
        var col_id = c;
        if (debug) {console.log('ID found at column ' + c)}

        break;

      case 'Client Secret':

        secretcolfound = true;
        var col_secret = c;
        if (debug) {console.log('secret found at column ' + c)}

        break;

      case '':

        blankcolfound = true;
        
        break;

      default:
		  
		    //do nothing
		  
	  }
    
    c++;

  }

  allfound = (usercolfound && appcolfound && shtidcolfound && idcolfound && secretcolfound);
  var foundrow = false;

  if (allfound) {
    //find row / username of owner
    var r = row_header + 1;
    var reachedblankrow = false;
    var row_user = 0;
    
    while (!foundrow && !reachedblankrow && r < 1023) {
      
      if (sht_PublicKeys.getRange(r, col_user).getValue() == usr_Super.email && sht_PublicKeys.getRange(r,col_app).getValue() == props.thisApp.getProperty('App Name')) {
        
        foundrow = true;
        row_user = r;
        if (debug) {console.log('user found at row ' + r)}
              
      } else if (sht_PublicKeys.getRange(r,col_user).getValue() == '' ) {

        reachedblankrow = true;
        if (debug) {console.log('Matching Key Row Not Found, values are: ' + sht_PublicKeys.getRange(r, col_user).getValue() + ' == ' + usr_Super.email + ' & ' + sht_PublicKeys.getRange(r,col_app).getValue() + ' == ' + props.thisApp.getProperty('App Name'))}

      } else {

        //do nothing

      }

      r++;

    }
  }

  //4. Get user API sheet ID from same row
  if (foundrow) {

    c = 1;
    var thiskey = '';
    var thisvalue = '';
    var endofrow = false;

    var col_id = 0;
    var col_secret = 0;

    while (!endofrow) {

      thiskey = sht_PublicKeys.getRange(row_header,c).getValue();
      if (thiskey != '') {
        
        thisvalue = sht_PublicKeys.getRange(row_user,c).getValue();
        props.superUser.setProperty(thiskey, thisvalue);
        if (thiskey == 'Client ID') {

          col_id = c;

        } else if (thiskey == 'Client Secret') {

          col_secret = c;

        }
        c++;

      } else {

        endofrow = true;

      }

    }
  } else {
    
    //Error, always log
    console.log('Proper Credentials Not Found');

  }

  //5. Lookup User API sheet from sheet ID - again match username & project name
  if (props.superUser.getProperty('User Key Sheet ID') != null) {

    var ss_OwnerKeys = SpreadsheetApp.openById(props.superUser.getProperty('User Key Sheet ID'));
    var sht_OwnerKeys = ss_OwnerKeys.getSheetByName(name_shtKeys);

    creds.id = sht_OwnerKeys.getRange(row_user, col_id).getValue();
    creds.secret = sht_OwnerKeys.getRange(row_user, col_secret).getValue();

    props.superUser.setProperty('Client ID',creds.id);
    props.superUser.setProperty('Client Secret',creds.secret);

  }
    
  //6. Import Client ID and Client Secret from same row

  creds.success = (creds.id != null && creds.secret != null);

  return creds;

}

//**************** START Generic API Functions ****************\\


//************** START SMARTSHEET API SPECIFIC FUNCTIONS **************\\

function pullSmartSheet(thisform) {
  
  const str_method = 'get';
  
  if (thisform == null) {thisform = {}}
  if (thisform.name == null) {thisform.name = "ApiApiJoyJoy"}

  var debug = modeDebug();

  //Get API object with keywords and necessary values
  var obj_APISmartsheet = getAPIobject('SmartSheet');
  
  //Create distinct object which will also include JSON commands in addition to API variables
  var obj_command = obj_APISmartsheet;
  var options = {};

  obj_command['method'] = str_method;

  var service = getService();
  if (service.hasAccess()) {

    options['method'] = str_method;
    options['contentType'] = obj_command['contentType'];
    options.headers = obj_command.headers;
    
    var obj_request = UrlFetchApp.getRequest(obj_command.url[str_method],options);
    if (debug) {console.log(obj_request)}
    var list_Sheets = UrlFetchApp.fetch(obj_command.url[str_method],options);
    if (debug) {console.log(list_Sheets.getContentText())}

  } else {

    //Prompt user stating service failed and the reason why

    //Access error, always log
    console.log('User does not have authorization')

  }

}

function getSmartSheetList(name) {

  var debug = modeDebug();

  txt_Sheets = new String; //JSON string to return from function
  var list_Sheets = {}; //Create new JSON object variable for return object
  list_Sheets.data = []; //Object needs a "data" array to parse / distinguish headers from data correctly

  const str_method = 'get';   //This will be an HTTPS "GET" Command

  //Information about the service being interfaced with and appropriate fields for the RESTful / JSON API
  if (name == null) {name = "MetaProject SS"}
  var obj_APISmartsheet = getAPIobject('SmartSheet');
  
  //Create a Superset of the Commands, including get
  var obj_command = obj_APISmartsheet;
  obj_command['method'] = str_method;  
  
  //Created a subset to be passed in the Get Request
  var options = {};

  //Verify Service is available
  var service = getService();

  if (service.hasAccess()) {

    //Set up the options required for this request
    options['method'] = str_method;
    options['contentType'] = obj_command['contentType'];
    options.headers = obj_command.headers;
    
    //These two lines were for testing to see the actual text request being made and inspect it
    //var obj_request = UrlFetchApp.getRequest(obj_command.url[str_method],options);
    if (debug) {console.log(obj_request)}
    
    //Finally, make the actual command to get the list of sheets
    txt_Sheets = UrlFetchApp.fetch(obj_command.url[str_method],options);
    if (debug) {console.log(list_Sheets.getContentText())}

  } else {

    //Prompt user stating service failed and the reason why
    //Future: Depending on error can prompt / provide links to correct
    var authorizationUrl = service.getAuthorizationUrl();
    if (debug) {console.log('Open the following URL and re-run the script: %s',
        authorizationUrl)} //For some reason, when ran here, it is not giving the correct authorizationURL. But when run from Smartsheet.gs, it is correct. Why? Is it getting the wrong script properties?
    list_Sheets.errormsg = 'User is not Currently Authorized'
    list_Sheets.data[0] = {'permalink': authorizationUrl, 'id':'Open this link and then re-run script if successful'};
    txt_Sheets = JSON.stringify(list_Sheets);

  }

  //Pass the JSON formatted list of sheets to the calling function
  //return list_Sheets.getContentText();
  return txt_Sheets;

}

//**************** END SMARTSHEET API SPECIFIC FUNCTIONS ****************\\

//************** START CREDENTIAL / AUTHORIZTION FUNCTIONS **************\\

function setAPIcredentials (temp_Creds, altAppProps) {

  /**
   * temp_Creds is an HTML formatted template for use to output the form
   **** ApiApiJoyJoy should not contain any GUI elements, so this GUI element is created by the calling app and passed for use/processing
   **** The calling App should include all necessary elements in the template to accomoate all property stores in altAppProps in addition to the ApiApiJoyJoy (Script) Property Store
   * altAppProps is an array of property stores that contains the property stores of all apps requesting access
  **/

  var ui = SpreadsheetApp.getUi();
  var debug = modeDebug();
  var oldcreds = getAPIcredentials();

  //Initialize new credentials object
  var newcreds = {};
  
  if (temp_Creds != null) {

    //Future: Need to check/verify passed template has enough elements to accomoadate altAppProps

    //Must first create a template with fields that can be replaced by this script
    if (oldcreds.id.length > 0) {temp_Creds.idvalue = oldcreds.id}
    if (oldcreds.secret.length > 0) {temp_Creds.secretvalue = oldcreds.secret}

    //var form_creds = HtmlService.createHtmlOutputFromFile(name_CredsForm);
    //Once the template is populated, we render/evaluate the template and create the actual HTML [Required for Google API security features, or else everything is "undefined"]
    var form_creds = temp_Creds.evaluate().setSandboxMode(HtmlService.SandboxMode.IFRAME);

    form_creds.setWidth(600)
    form_creds.setHeight(200)

    newcreds = ui.showModalDialog(form_creds,'API Credentials');
    //Form is set to run setCredsfromForm when user hits "submit" and runs nothing if cancelled

  } else {
    //This is a backup method of getting the user input that doesn't use HTML form
    //This will ONLY set the credentials for ApiApiJoyJoy and not for anything else
    
    var txt_prompt = '';

    if (oldcreds.id.length > 0) {
      txt_prompt = 'Enter Client ID (existing: ' + oldcreds.id + '):';
    } else {
      txt_prompt = 'Enter Client ID:';
    }

    var newid = ui.prompt(txt_prompt);
    
    if (newid.getResponseText().length > 0) {

      newcreds.id = newid.getResponseText();

    } else {
      
      newcreds.id = oldcreds.id;
      txt_prompt = 'Existing Value Retained';
      //ui.alert(txt_prompt);
      if (debug) {console.log(txt_prompt)}

    } 

    if (oldcreds.secret.length > 0) {
      txt_prompt = 'Enter Client Secret (existing: ' + oldcreds.secret + '):';
    } else {
      txt_prompt = "Enter Client Secret:";
    }

    var newsecret = ui.prompt(txt_prompt);
    if (newsecret.getResponseText().length > 0) {

      newcreds.secret = newsecret.getResponseText();

    } else {

      newcreds.secret = oldcreds.secret;
      txt_prompt = 'Existing Value Retained';
      //ui.alert(txt_prompt);
      if (debug) {console.log(txt_prompt)}

    }

    props = {};
    props.app = {};
    props.app.ApiApiJoyJoy = PropertiesService.getScriptProperties();
    props.superUser = PropertiesService.getUserProperties();
    props.superUser.setProperty('Client ID',newcreds.id);
    props.superUser.setProperty('Client Secret', newcreds.secret);

  }

}

function getAPIcredentials() {

  var creds = {};
  var props = {};
  var thisUser = getUserObject();
  if (thisUser.group == 'admin') {

    var superUser = thisUser;
    props.superUser = PropertiesService.getUserProperties();
  
  }

  props.user = PropertiesService.getUserProperties();
  props.thisApp = PropertiesService.getScriptProperties();
  var debug = (props.thisApp.getProperty('Debug Mode') == 'True');

  //At some point, need to add a check for authorization type - OAuth2 vs. other possible methods

  if (true) { //Need to add a check that user is SuperUser
    //Authorizations for Super Users
    props.superUser = props.user;
    creds.id = props.superUser.getProperty('Client ID')
    creds.secret = props.superUser.getProperty('Client Secret')
    creds.token = props.thisApp.getProperty('App Token')

    creds.success = (creds.id != null && creds.secret != null);

  } else {
    
    //Authorizations for Lower Level Users
    creds.id = props.user.getProperty('Client ID');
    creds.secret = props.user.getProperty('Client Secret');

  }

  if (debug) {
    console.log('Client ID: ' + creds.id);
    console.log('Client Secret: ' + creds.secret);
    console.log('Access Token: ' + creds.token);
  }

  return creds;

}