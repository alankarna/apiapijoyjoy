/**
 * APITables.gs/js
 * Functions that act as variable stores for generic variables and api specific variables
 * Future state: not hardcoded, functions to set / clear / update / reset to default scriptproperties store for this app - can be customized for users needs.
 * Set functions would basically set defaults, then leave user to add as needed.
*/

/**
 * Class Definitions
 */

class Credentials {
  /**
   * Credentials class
   * type is required input to create new Credentials
   * Stores Authorization information based on type of Authorization
   */
  constructor(type, id, secret, token) {
       
    this.type = type;
    
    //Future: Other parts of constructor determined by type
    switch (type) {
      case 'OAuth2':
      
      default:
        if (id == null || secret == null || token == null) {
          this.id = new String;
          this.secret = new String;
          this.token = new String;
        } else {
          this.id = id;
          this.secret = secret;
          this.token = token;
        }

    }
    Object.seal(this); //Prevents adding new or removing any extensions to object without editing this class
  }

}

class Modes {
  /**
   * Modes Class
   * store for different mode switches (e.g. Debug) for the app
   */
  constructor () {

    this.debug = false;
    Object.seal(this); //Prevents adding new or removing any extensions to object without editing this class

  }

}

class Api {

  constructor(name) {

    this.name = name;
    this.mode = new Modes;
    this.creds = new Credentials;
    Object.seal(this); //Prevents adding new or removing any extensions to object without editing this class
  }
  
}

class ApiCollection {

  constructor() {

    var _array = []; //Internal Static Private variable, or the best JavaScript can offer for one. Not sure how it stays static, but it does somehow

    this.addApi = function(api) {
      /**
       * "Setter" for Api object collection, adds an Api object to the collection
       * If 'api' is a proper Api Class object, will push to array
       * If 'api' is a string, treats the string as the name of a new Api Class Object
       * If neither, should generate an error
       */
      if (api instanceof Api) {

        _array[api.name] = api;

      } else if (typeof api == 'string') {
      
        _array[api] = new Api(api);
      
      } else {

        //Should generate an error or message to user here

      }
      
    }

    this.getApi = function(index) {
      /**
       * "Getter" for Api object collection
       * If 'index' is a proper number, will return array object of that index
       * If 'index' is null, returns the entire array
       * If 'index' is other, should throw an error (future, not implemented yet)
       */
      if (typeof index  == 'number' && index == index.toFixed(0)) {
        //Index must be number type and be an integer
        return _array[index];

      } else if (typeof index == 'string') {
        //Future: Need to verify the string is actually a key used in the Api
        return _array[index];
      } else {
        //Future: specifically check for null and return full array but if invalid, throw error
        return _array;

      }
    }

    Object.freeze(this); //Prevents adding new or removing any extensions to object, or worse - overwriting the add and/or get functions, without editing this class

  }

}

class App {

  constructor(name) {

    this.name = name;
    this.mode = new Modes;
    this.api = new ApiCollection;
    Object.seal(this); //Prevents adding new or removing any extensions to object without editing this class

  }

}

class AppCollection {

  constructor() {

    var _array = []; //Internal Static Private variable, or the best JavaScript can offer for one. Not sure how it stays static, but it does somehow

    this.addApp = function(app) {
      /**
       * "Setter" for Api object collection, adds an Api object to the collection
       * If 'api' is a proper Api Class object, will push to array
       * If 'api' is a string, treats the string as the name of a new Api Class Object
       * If neither, should generate an error
       */
      if (app instanceof App) {

        _array[app.name] = app;

      } else if (typeof app == 'string') {
      
        _array[app] = new App(app);
      
      } else {

        //Should generate an error or message to user here

      }
      
    }

    this.getApp = function(index) {
      /**
       * "Getter" for Api object collection
       * If 'index' is a proper number, will return array object of that index
       * If 'index' is null, returns the entire array
       * If 'index' is other, should throw an error (future, not implemented yet)
       */
      if (typeof index  == 'number') {
        //Future: Further verify that number is an integer
        return _array[index];

      } else if (typeof index == 'string') {
        //Future: Need to verify the string is actually a key used in the Api
        return _array[index];
        
      } else {
        //Future: specifically check for null and return full array but if invalid, throw error
        return _array;

      }
    }

    Object.freeze(this); //Prevents adding new or removing any extensions to object, or worse - overwriting the add and/or get functions, without editing this class

  }

}

class User {

  constructor(name) {

    

  }


}

/**
 * Test Function only for testing classes - will be deleted
 */

function testthis () {

  //var mixedarray = [5, 'hello'];
  var apps = new App('ApiApiJoyJoy');
  apps.api.addApi('SmartSheet');
  var apiTC = new Api('TeamCenter');
  apps.api.addApi(apiTC);
  apps.api.getApi = 'ha, now your getter is overwritten';
  
  console.log(apps);
  console.log(apps.api.getApi());
  console.log(apps.api);
  //console.log(mixedarray);

}

/**
 * Begin actual API Functions
 */

function getAPIobject(apiName) {

  var obj_API = {};
  
  switch (apiName) {

    case 'SmartSheet':
      obj_API = apiSmartSheet();

    break;

    default:
      //do nothing, returns empty object

  }

  return obj_API;

}

function expProperties(array_apps, array_apis) {
  //Maintains a list of Expected Properties required for proper function of the API
  //Check against this list to validate your code / validate current set of properties is correct for this version
  //Primary purpose - to rerun initialization in case of missing or invalid properties
  let props_Exp = {};

  const name_thisApp = 'ApiApiJoyJoy';
  
  var array_allaps = [];
  array_allaps = array_allaps.concat(name_thisApp,array_apps); //Creates an array of all involved apps

  //List of properties expected to be contained in an App Property store
  props_Exp.app = {};
  for (var app = 0; app < array_allaps.length; app++) {

    let thisapp = array_allaps[app];
    props_Exp.app[thisapp] = [
      'App Name',       //Name of the App
      'Debug Mode',     //Flag for Debug messages
      'Client ID',      //Client ID of app to access an API
      'Client Secret',  //Secret Key of app to access an API
      'Access Token',   //Access Token of app to access an API
      'API'             //List of Names of API that the app has access to / perhaps array of API objects
    ];

  }

  //Top level API objects
  //Tracks all APIs that ApiApiJoyJoy can currently support
  props_Exp.api = {};
  for (var api = 0; api < array_apis; api++) {
    let thisapi = array_apis[api];
    props_Exp.api[thisapi] = [
      'Api Name',       //Name of the api
      'Debug Mode',     //Flag for debug messages
      'Client ID',      //Client ID of app to specifically access this API
      'Client Secret',  //Secret Key of app to specifically access this API
      'Access Token'    //Access Token for this app to specifically access this API
    ];

  }

  props_Exp.user = [
    'User',
    'email',
    'Public Key Sheet ID',
    'User Key Sheet ID'
  ];

  props_Exp.doc = [

  ]; //Currently no required Document Properties
  
  return props_Exp;

}

function apiGeneric(cmd) {

  //Tracks generic keywords - when matched with keywords in other APIs, they perform specific actions
  hash_API = {};
    hash_API.keywords = {};
      hash_API.keywords['hyperlink'] = true; //Hyperlink to the object
      hash_API.keywords['id'] = true; //ID number of the object
      hash_API.keywords['name'] = true; //Name of the object
      hash_API.keywords['accessUser'] = true; //User's Access Level
      hash_API.keywords['dateCreated'] = true; //Date file/entry was created
      hash_API.keywords['dateModified'] = true; //Date file/entry was last modified
      hash_API.keywords['isBookmarked'] = true; //User has bookmarked this item

  return hash_API;

}

function apiSmartSheet(cmd) {
  
  //SmartSheet API Information
  
  //Set up Associative (Hash) Table Entry
  var hash_SS = {};

  //Relates any keywords for this specific API to the generic keywords
  hash_SS.keywords = {};
  hash_SS.keywords['permalink'] = 'hyperlink';
  hash_SS.keywords['id'] = 'id';
  hash_SS.keywords['name'] = 'name';
  hash_SS.keywords['accessLevel'] = 'accessUser';
  hash_SS.keywords['createdAt'] = 'dateCreated';
  hash_SS.keywords['modifiedAt'] = 'dateModified';
  hash_SS.keywords['favorite'] = 'isBookmarked';

  //List of common API scopes
  hash_SS.scope = {};
  hash_SS.scope['read'] = 'READ_SHEETS';

  //Set Main Properties
  hash_SS['name'] = 'SmartSheet';
  hash_SS['contentType'] = 'application/json';
  hash_SS['authType'] = 'OAuth2';

  //Get Client ID and Secret from the user properties (should have been set in the calling script at some point)
  //This could be an issue as not all users will have ID/Secret. However, using ID/Secret in the script could reveal individual credentials.
  //Can access token be used instead?

  //Future: Should loop through standard/generic names and assign hash property to user/script properties by generic name
  hash_SS['id'] = PropertiesService.getUserProperties().getProperty('Client ID');
  hash_SS['secret key'] = PropertiesService.getUserProperties().getProperty('Client Secret');
  hash_SS['app token'] = PropertiesService.getScriptProperties().getProperty('App Token');
  hash_SS['authorization'] = 'Bearer ' + hash_SS['App Token'];

  //List of URLs for API calls
  hash_SS.url = {};
  hash_SS.url['main'] = 'https://api.smartsheet.com/2.0/users/me';
  hash_SS.url['token'] = 'https://api.smartsheet.com/2.0/token';
  hash_SS.url['auth'] = 'https://app.smartsheet.com/b/authorize';
  hash_SS.url['get'] = 'https://api.smartsheet.com/2.0/sheets';
  
  //List of user properties
  hash_SS.user = {};
  hash_SS.user['name'] = '';
  hash_SS.user['email'] = '';
  hash_SS.user['level'] = '';

  //List of common API commands / functions
  hash_SS.cmd = {};
  hash_SS.cmd['callback'] = 'authCallback';

  //Authorization Headers
  //Only set with keys with Capital letters
  hash_SS.headers = {};
  hash_SS.headers['Authorization'] = hash_SS['authorization'];
  hash_SS.headers['Content-Type'] = hash_SS['contentType'];

  if (cmd == null) {

    return hash_SS;
  
  } else {

    return hash_SS[cmd];

  }

}
